#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""TP PRNG."""

# Generate documentation: epydoc -v --html main.py -o ./docs
# Make tests: python3 -m doctest -v main.py

import time

from matplotlib import pyplot
from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '29 March 2018'

class LinearCongruentialGenerator(object):
    """Linear Congruential Generator utils."""
    def __init__(self, seed=0, modulus=32, a_multiplier=1664525, b_increment=1013904223):
        """Constructor of I{LinearCongruentialGenerator}.

        @param self: Current instance of I{LinearCongruentialGenerator}.
        @type self: C{LinearCongruentialGenerator}

        @param seed: seed of LCG PRNG.
        @type seed: C{int}
        @raise seed: TypeError, seed must be a valid integer (C{int}).

        @param modulus: modulus of LCG PRNG.
        @type modulus: C{int}
        @raise modulus: TypeError, modulus must be a valid integer (C{int}).

        @param a_multiplier: a multiplier of LCG PRNG.
        @type a_multiplier: C{int}
        @raise a_multiplier: TypeError, a multiplier must be a valid integer (C{int}).

        @param b_increment: b increment of LCG PRNG.
        @type b_increment: C{int}
        @raise b_increment: TypeError, b increment must be a valid integer (C{int}).
        """
        if not isinstance(seed, int):
            raise TypeError('seed must be a valid integer!')
        elif not isinstance(modulus, int):
            raise TypeError('modulus must be a valid integer!')
        elif not isinstance(a_multiplier, int):
            raise TypeError('a multiplier must be a valid integer!')
        elif not isinstance(b_increment, int):
            raise TypeError('b increment must be a valid integer!')
        else:
            self.seed = seed
            self.modulus = modulus
            self.a_multiplier = a_multiplier
            self.b_increment = b_increment

    def generate(self):
        """Generate a PRNG using Liner Congruential Generator."""
        self.seed = (self.a_multiplier * self.seed + self.b_increment) % (2**self.modulus)
        return self.seed

def compute_period(lcg):
    """Compute period of linear congruential generator.

    @param lcg: Instance of I{LinearCongruentialGenerator}.
    @type lcg: C{LinearCongruentialGenerator}
    """
    i = 0
    found = False
    rand_numbers = []

    while not found:
        rand_ = lcg.generate()
        if rand_ not in rand_numbers:
            rand_numbers += [rand_]
            i += 1
        else:
            found = True

    return (i, rand_numbers)

def show_distrib(lcg, count=1000):
    """Show distribution of PRNG.

    @param lcg: Instance of I{LinearCongruentialGenerator}.
    @type lcg: C{LinearCongruentialGenerator}

    @param count: Number of PRN to generate with I{LinearCongruentialGenerator}.
    @type count: C{int}
    """
    # Generate 1000 PRNG using LCG and draw a chart to show them.
    x = []
    y = []
    for i in range(count):
        x += [i]
        y += [lcg.generate()]

    pyplot.scatter(x, y)
    pyplot.xlabel('Count')
    pyplot.ylabel('Random number')
    pyplot.title('Linear Congruential Generator')
    pyplot.show()

def main():
    """Main function."""
    try:
        # Create linear congruential generator
        # lcg = LinearCongruentialGenerator(int(time.time()))
        lcg = LinearCongruentialGenerator(5, 24, 6, 2)

        log.success('Linear Congruential Generator intialized:')
        log.bold('seed: {}'.format(lcg.seed))
        log.bold('modulus: {}'.format(lcg.modulus))
        log.bold('a multiplier: {}'.format(lcg.a_multiplier))
        log.bold('b increment: {}'.format(lcg.b_increment))

        period, rand_numbers = compute_period(lcg)
        log.bold('computed period: {}'.format(period))
        log.bold('random numbers: {}'.format(rand_numbers))

        lcg = LinearCongruentialGenerator(3, 16, 7, 3)

        log.success('Linear Congruential Generator intialized:')
        log.bold('seed: {}'.format(lcg.seed))
        log.bold('modulus: {}'.format(lcg.modulus))
        log.bold('a multiplier: {}'.format(lcg.a_multiplier))
        log.bold('b increment: {}'.format(lcg.b_increment))

        period, rand_numbers = compute_period(lcg)
        log.bold('computed period: {}'.format(period))
        log.bold('random numbers: {}'.format(rand_numbers))

        # Show distribution of PRNG
        lcg = LinearCongruentialGenerator(5, 24, 6, 2)
        show_distrib(lcg, 1000)

        lcg = LinearCongruentialGenerator(3, 16, 7, 3)
        show_distrib(lcg, 1000)
    except (KeyError, TypeError, ValueError) as exception_:
        log.error(exception_)

# Runtime processor
if __name__ == '__main__':
    main()
