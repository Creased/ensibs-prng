/**
 * \file function.c
 * \brief PRNG.
 * \author Baptiste MOINE <contact@bmoine.fr>
 * \version 0.1-dev
 * \date 27 mars 2018
 */

#include <stdio.h>
#include "function.h"

void print_binary_value(int const size, void const * const ptr) {
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    /* sizeof(int) = 4 -> on doit boucler 8 fois pour atteindre les 32 bits */
    for (i=size-1; i>=0; i--) {
        for (j=7; j>=0; j--) {
            byte = (b[i] >> j) & 1;
            printf("%u", byte);
        }
    }
    printf("\n");
}
