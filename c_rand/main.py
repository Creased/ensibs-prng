#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""TP PRNG."""

# Generate documentation: epydoc -v --html main.py -o ./docs
# Make tests: python3 -m doctest -v main.py

import json

from matplotlib import pyplot
from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '29 March 2018'

def show_distrib(random_numbers):
    """Show distribution of PRNG."""
    i = 0
    x = []
    y = []

    for random_number in random_numbers:
        x += [i]
        y += [random_numbers[random_number]]
        i += 1

    pyplot.scatter(x, y)
    pyplot.xlabel('Count')
    pyplot.ylabel('Random number')
    pyplot.title('ANSI-C rand()')
    pyplot.show()

def lsb_msb_count(random_numbers):
    """Show LSB and MSB count."""
    x = []
    y = []
    count = {'lsb': {'0': 0, '1': 0}, 'msb': {'0': 0, '1': 0}}

    for random_number in random_numbers:
        # log.raw('{}\n'.format(random_numbers[random_number]))
        bin_value = '{pad}{number}'.format(pad='0'*32, number=bin(random_numbers[random_number])[2:])[-32:]
        log.raw('{}\n'.format(bin_value))
        lsb = bin_value[1]
        msb = bin_value[0]
        count['lsb'][lsb] += 1
        count['msb'][msb] += 1

    return count

def load_random_numbers(file):
    """Load random numbers from file."""
    config = dict()

    if file:
        try:
            with open(file, 'rt') as fdesc:
                random_numbers = json.load(fdesc)
                fdesc.close()
        except OSError:
            log.error('Specified file not found!')
        except ValueError as e:
            log.error(e)
            log.error('An error occured while parsing JSON file.')
        else:
            log.success('Loading random numbers from {file}.'.format(file=file))

    return random_numbers

def validate_config(conf, needed_keys):
    """Validate and return configuration."""
    if conf:
        for key in needed_keys:
            if key['id'] not in conf:
                log.error('{name} is missing, please check '
                          'your configuration file!'.format(name=key['id']))
            elif conf[key['id']] == '':
                log.error('{name} can\'t be empty, please check '
                          'your configuration file!'.format(name=key['name']))

    return conf

def main():
    """Main function."""
    try:
        random_numbers = load_random_numbers('random_numbers.json')
        log.success('LSB/MSB values distrib: {}'.format(lsb_msb_count(random_numbers)))
        show_distrib(random_numbers)
    except (KeyError, TypeError, ValueError) as exception_:
        log.error(exception_)

# Runtime processor
if __name__ == '__main__':
    main()
