#!/usr/bin/env python3
# -*- coding:utf-8 -*-

"""Simplified RC4 Python."""

# Generate documentation: epydoc -v --html main.py -o ./docs
# Make tests: python3 -m doctest -v main.py

import traceback

__author__ = 'Baptiste MOINE <contact@bmoine.fr>, Romain KRAFT <romain.kraft@protonmail.com>'
__version__ = '0.1-dev'
__date__ = '30 march 2018'

def arc_four(plaintext):
    i = 0
    j = 0

    S = [ord(i) for i in plaintext]
    plain_len = len(S)
    cipher_list = []

    for l in range(plain_len):
        i = (i + 1) % 256
        j = (j + S[i]) % 256
        S[i], S[j] = S[j], S[i]
        cipher_list.append(S[(S[i] + S[j]) % 256])

    cipher = ''.join([chr(i) for i in cipher_list])

    return cipher

def arc_four_wo_permut(S, length=0):
    i = 0
    j = 0

    if length == 0: length = len(S)
    cipher_list = []

    for l in range(length):
        i = (i + 1) % 256
        j = (j + S[i]) % 256
        # S[i], S[j] = S[j], S[i]
        cipher_list.append(S[(S[i] + S[j]) % 256])

    print('{}'.format(cipher_list[0:512]))
    print('{}'.format(cipher_list[512:1024]))

    return cipher_list

def main():
    """Main function."""
    try:
        input = [i for i in range(256)]
        result = arc_four_wo_permut(input, 1024)
    except (ValueError, TypeError, OSError) as exception_:
        print(traceback.format_exc())
        print(exception_)

# Runtime processor
if __name__ == '__main__':
    main()
