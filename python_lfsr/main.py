#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""TP LFSR."""

# Generate documentation: epydoc -v --html main.py -o ./docs
# Make tests: python3 -m doctest -v main.py

from collections import deque
from matplotlib import pyplot
from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '29 March 2018'

class LFSR(object):
    """Linear Feedback Shift Register PRNG utils."""
    def __init__(self, formula=[1, 0, 0, 1, 0, 1, 0, 0, 0], register=[1, 0, 0, 1, 0, 1, 0, 0, 0]):
        """Constructor of I{LFSR}.

        @param formula: LFSR formula (L1, L2, L3, ...)
        @type formula: C{list}

        @param register: LFSR register
        @type register: C{list}
        """
        if not isinstance(formula, list):
            raise TypeError('formula must be a valid list!')
        elif not isinstance(register, list):
            raise TypeError('register must be a valid list!')
        else:
            self.formula = formula
            self.register = deque(register)

    def init_register(self, register=[1, 0, 0, 1, 0, 1, 0, 0, 0]):
        """Define LFSR register."""
        if not isinstance(register, list):
            raise TypeError('register must be a valid list!')
        else:
            self.register = deque(register)

    def next(self):
        """Generate next bn value and update register."""
        result = 0;

        for i in range(len(self.formula)):
            if (self.formula[i] == 1):  # process this cell (or not) based on formula
                result = result ^ self.register[i]

        # Right shifting
        self.register.appendleft(result) # append result to the left side of the register
        self.register.pop()  # remove right most value from the register

        return self.register

    def generate(self):
        """Generate random number using LFSR."""
        result  = 0

        self.next()

        for i in range(len(self.formula)):
            result += pow(2, i) * self.register[len(self.formula) - i - 2]

        result = float(result) / float(pow(2, len(self.formula)))

        return result

def show_distrib(lfsr, count=1000):
    """Show distribution of PRNG.

    @param lfsr: Instance of I{LFSR}.
    @type lfsr: C{LFSR}

    @param count: Number of PRN to generate with I{LFSR}.
    @type count: C{int}
    """
    # Generate 1000 PRNG using LFSR and draw a chart to show them.
    x = []
    y = []
    for i in range(count):
        x += [i]
        y += [lfsr.generate()]

    pyplot.scatter(x, y)
    pyplot.xlabel('Count')
    pyplot.ylabel('Random number')
    pyplot.title('LFSR')
    pyplot.show()

def main():
    """Main function."""
    try:
        # Create LFSR
        lfsr = LFSR([0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0],
                    [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0]);

        log.success('LFSR intialized:')
        log.bold('seed: {}'.format(lfsr.register))
        log.bold('formula: {}'.format(lfsr.formula))

        # Show distribution of PRNG
        show_distrib(lfsr, 1000)
    except (KeyError, TypeError, ValueError) as exception_:
        log.error(exception_)

# Runtime processor
if __name__ == '__main__':
    main()
