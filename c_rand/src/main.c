/**
 * \file function.c
 * \brief PRNG.
 * \author Baptiste MOINE <contact@bmoine.fr>
 * \version 0.1-dev
 * \date 27 mars 2018
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "function.h"

/** Fonction principale.
 *
 * \param argc Nombre d'arguments.
 * \param argv Tableau d'arguments.
 *
 * \return 0 Le s'est exécuté sans rencontrer d'erreurs.
 */
int main(int argc, char **argv) {
    int rand_, seed_, i;
    FILE *fdesc;
    char *fname = "random_numbers.json";

    fdesc = fopen(fname, "w");

    if (fdesc == NULL) {
         printf("Can't open %s for writing.\n", fname);
    } else {       
        seed_ = time(NULL);
        srand(seed_);

        printf("Seed: %d\n", seed_);
        printf("Max random value: %d\n", RAND_MAX);
        printf("Output: %s\n\n", fname);

        fprintf(fdesc, "{\n");

        for (i=0; i < 100000; i++) {
            rand_ = rand();

            fprintf(fdesc, " \"%d\": %d", i, rand_);
            if (i != 99999) {
                fprintf(fdesc, ",");
            }
            fprintf(fdesc, "\n");

            /* printf("%-10d => ", rand_);
            print_binary_value(sizeof(rand_), &rand_); */
        }
        fprintf(fdesc, "}");

        fclose(fdesc);
    }

    return 0;
}
