/**
 * \file function.h
 * \brief PRNG.
 * \author Baptiste MOINE <contact@bmoine.fr>
 * \version 0.1-dev
 * \date 27 mars 2018
 */

#ifndef __FUNCTION_H
#define __FUNCTION_H

/** Prototype des fonctions de function.c. */
void print_binary_value(int const size, void const * const ptr);

#endif
