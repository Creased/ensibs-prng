# TD PRNG - Qualité d'un générateur pseudo aléatoire

 - Date&nbsp;: 15/05/2018
 - Auteur&nbsp;: [Baptiste MOINE <contact@bmoine.fr>](contact@bmoine.fr)

## Question 1 - Implémentation d'un PRNG linéaire congruentiel

L'objectif de cette question est l'implémentation d'un générateur aléatoire basé sur les congruences linéaires&nbsp;:

<center>
<img src="http://www.sciweavers.org/tex2img.php?bc=White&fc=Black&im=png&fs=12&ff=arev&eq=f%28k%29%20%3D%20a%20%2A%20k%20%2B%20b%20%28mod%202%5Em%29" alt="f(k) = a * k + b (mod 2^m)" />
</center>

Le code ainsi obtenu est décrit [ici](python_lcg/docs/index.html).


## Question 2 - Calcul de période

Par définition, ce générateur aléatoire est périodique puisqu'il s'appuie sur les
principes de la modularité pour générer une suite.

De plus, chaque terme de cette suite (compris entre 1 et `2^m-1`) dépend entièrement du précédent.
Cette suite est donc périodique et donc amenée à se répéter au bout d'un certain nombre d'itération.

Pour calculer sa période, il suffit donc de compter les nombres jusqu'à en obtenir
un que l'on a déjà généré. Par exemple, pour le générateur `f(k) = a * k + b (mod 2^m)`,
avec `k=5`, `m=24`, `a=6` et `b=2` et on obtient&nbsp;:

```python
# pushd python_lcg/
# python main.py
[+] Linear Congruential Generator intialized:
seed: 5
modulus: 24
a multiplier: 6
b increment: 2
computed period: 24
random numbers: [32, 194, 1166, 6998, 41990, 251942, 1511654, 9069926, 4087910, 7750246, 12947046, 10573414, 13108838, 11544166, 2156134, 12936806, 10511974, 12740198, 9332326, 5662310, 419430, 2516582, 15099494, 6710886]
# popd
```

La période est ici de 24, mais avec `k=3`, `m=16`, `a=7` et `b=3`, la période est de 16384&nbsp;!

En d'autres termes, il ne s'agit pas de prendre un `m` excessivement grand pour obtenir une
suite disposant d'une période suffisamment grande&nbsp;:

```python
# pushd python_lcg/
# python main.py
[+] Linear Congruential Generator intialized:
seed: 3
modulus: 16
a multiplier: 7
b increment: 3
computed period: 16384
[...]
# popd
```

<center>
 <img src="./illustrations/lcg_1.png" />
 <p><i>Figure 1&nbsp;: PRNG LCG avec k=5, m=24, a=6 et b=2</i></p>
</center>

<center>
 <img src="./illustrations/lcg_2.png" />
 <p><i>Figure 2&nbsp;: PRNG LCG avec k=3, m=16, a=7 et b=3</i></p>
</center>


### Question 3 - Étude de la fonction `rand(3)`

> The versions of rand() and srand() in the Linux C Library use the same random number generator as random(3) and srandom(3), so the lower-order bits should be as random as the > higher-order bits.
> However, on older rand() implementations, and on current implementations on different systems, the lower-order bits are much less random than the higher-order bits. Do not use this function in applications intended to be portable when good randomness is needed. (Use random(3) instead.)

D'après cet extrait du manuel de la fonction [`rand(3)`](https://linux.die.net/man/3/rand) de la libc,
la génération de nombres aléatoires présente quelques défauts notamment le fait que les bits de poids
faible seraient moins aléatoires que les bits de poids forts. Afin de le vérifier nous allons simplement
générer une suite de nombres aléatoires suffisamment conséquente et faire des observations.

Pour simplifier cette étude, j'ai fait le choix d'implémenter un petit programme en C qui génère une suite de
100000 nombres avec la fonction `rand(3)` et les stocke dans un [fichier JSON](c_rand/random_numbers.json).
Une fois généré, ce [fichier JSON](c_rand/random_numbers.json) peut directement être exploité en utilisant [Python](c_rand/main.py).

Dans un premier temps, vérifions la répartition binaire des bits à 1 et 0 sur le MSB et le LSB des nombres générés&nbsp;:


```
# pushd c_rand/
# python main.py
[+] LSB/MSB values distrib: {'lsb': {'1': 50077, '0': 49923}, 'msb': {'1': 0, '0': 100000}}
# popd
```

Au niveau des LSB, rien de bien choquant, on retrouve une répartition plutôt convenable des bits à 0 et 1
sur les LSB (observation faite sur 10 jeux d'essais). Quant au MSB on constate qu'il n'y a jamais de bits à 1...
Ceci s'explique rapidement par le fait qu'un entier sur une architecture 64 bits est représenté sur 32 bits, or
la plupart du temps on appliquera un padding sur nos nombres entiers afin de pouvoir observer des chaînes binaires
de tailles identiques. En outre, les entiers étant signés, un nombre possédant un bit à 1 en MSB sera interprété
comme un nombre négatif.

Nous ne sommes donc malheureusemet pas en mesure de retrouver les observations décrites dans la manuel de la fonction,
il faut toutefois rester prudent. En effet, s'agissant d'une fonction interne à la libc, celle-ci évolue constamment
et peut donc avoir subit une réimplémentation de sa fonction de génération de nombres aléatoires.

<center>
 <img src="./illustrations/c_rand.png" />
 <p><i>Figure 3&nbsp;: PRNG rand(3) de la libc</i></p>
</center>

Pour pousser un peu plus loin l'analyse de cette fonction de génération, nous pouvons utiliser des outils d'analyse FIPS.
Ceux-ci sont disponibles dans le Sequencer de la suite d'outils de [PortSwigger BurpSuite Pro](https://portswigger.net/burp).

#### FIPS monobit test

<center>
 <img src="./illustrations/monobit_test.png" />
 <p><i>Figure 4&nbsp;: Analyse de l'aléatoire sur tous les bits</i></p>
</center>

Comme on le voit sur ce graphique, la répartition des bits est plutôt mauvaise d'après cette analyse.
À noter que les 32 bits n'apparaissent pas pour éviter de prendre en compte les bits de padding
(ce qui est étrange à mon humble avis puisqu'il s'agit tout de même d'une information sur le générateur
utilisé...).

#### FIPS bit correlation test

<center>
 <img src="./illustrations/bit_correlation.png" />
 <p><i>Figure 5&nbsp;: Rercherche de relations entre les bits à différentes positions</i></p>
</center>

Avec cette analyse on constate que le bit à la position 25 a de forts liens avec le bit à la position 26,
ce qui peut affaiblir grandement la robustesse de notre suite de nombres aléatoires.

#### Conclusion

In fine, bien que l'observation sur les bits de poids faibles ne semble pas réellement
présente sur nos jeux d'essais, on peut toutefois constater que ce générateur n'est
en effet pas fiable pour une application cryptographique.

### Question 4 - Implémentation d'un PRNG LFSR

L'objectif de cette question est l'implémentation d'un générateur aléatoire basé sur un registre à décalage à rétroaction linéaire.

Le code ainsi obtenu est décrit [ici](python_lfsr/docs/index.html).

### Question 5 - Périodicité du LFSR

Les valeurs possibles dans un registre sont finies, ainsi, on retombera nécessairement sur un état déjà rencontré.
Par exemple, la périodicité maximale d'un LFSR sur 32 bits est de `2³² - 1 = 4 294 967 295`.

### Question 2 : Calculer à la main les bits générés par la `bn = bn - 4 ⊕ bn - 3`

Avec un état de départ ne contenant que des 1, on obtiens une périodicité de 23. En revanche,
avec un état de départ ne contenant que des 0, la périodicité est nulle puisque `0 ⊕ 0 = 0`.

### Comparaison des deux algorithmes PRNG

<center>
 <img src="./illustrations/lcg_1000.png" />
 <p><i>Figure 6&nbsp;: 1000 PRN générés par un PRNG LCG</i></p>
</center>

<center>
 <img src="./illustrations/lfsr.png" />
 <p><i>Figure 7&nbsp;: 1000 PRN générés par un PRNG LFSR</i></p>
</center>

D'après les observations, aucun générateur ne semble disposer d'une meilleure répartition que l'autre...

### Analyse de RC4 simplifié

L'implémentation du RC4 simplifiée est simple en Python et ne met en oeuvre que de simples modulos&nbsp;:

```python
def arc_four_wo_permut(S, length=0):
    i = 0
    j = 0

    if length == 0: length = len(S)
    cipher_list = []

    for l in range(length):
        i = (i + 1) % 256
        j = (j + S[i]) % 256
        # S[i], S[j] = S[j], S[i]
        cipher_list.append(S[(S[i] + S[j]) % 256])

    print('{}'.format(cipher_list[0:512]))
    print('{}'.format(cipher_list[512:1024]))

    return cipher_list

def main():
    input = [i for i in range(256)]
    result = arc_four_wo_permut(input, 1024)

if __name__ == '__main__':
    main()
```

Un fois exécuté, on obtient la sortie suivante&nbsp;:

```bash
[2, 5, 9, 14, 20, 27, 35, 44, 54, 65, 77, 90, 104, 119, 135, 152, 170, 189, 209, 230, 252, 19, 43, 68, 94, 121, 149, 178, 208, 239, 15, 48, 82, 117, 153, 190, 228, 11, 51, 92, 134, 177, 221, 10, 56, 103, 151, 200, 250, 45, 97, 150, 204, 3, 59, 116, 174, 233, 37, 98, 160, 223, 31, 96, 162, 229, 41, 110, 180, 251, 67, 140, 214, 33, 109, 186, 8, 87, 167, 248, 74, 157, 241, 70, 156, 243, 75, 164, 254, 89, 181, 18, 112, 207, 47, 144, 242, 85, 185, 30, 132, 235, 83, 188, 38, 145, 253, 106, 216, 71, 183, 40, 154, 13, 129, 246, 108, 227, 91, 212, 78, 201, 69, 194, 64, 191, 63, 192, 66, 197, 73, 206, 84, 219, 99, 236, 118, 1, 141, 26, 168, 55, 199, 88, 234, 125, 17, 166, 60, 211, 107, 4, 158, 57, 213, 114, 16, 175, 79, 240, 146, 53, 217, 126, 36, 203, 115, 28, 198, 113, 29, 202, 120, 39, 215, 136, 58, 237, 161, 86, 12, 195, 123, 52, 238, 169, 101, 34, 224, 159, 95, 32, 226, 165, 105, 46, 244, 187, 131, 76, 22, 225, 173, 122, 72, 23, 231, 184, 138, 93, 49, 6, 220, 179, 139, 100, 62, 25, 245, 210, 176, 143, 111, 80, 50, 21, 249, 222, 196, 171, 147, 124, 102, 81, 61, 42, 24, 7, 247, 232, 218, 205, 193, 182, 172, 163, 155, 148, 142, 137, 133, 130, 128, 127, 127, 128, 130, 133, 137, 142, 148, 155, 163, 172, 182, 193, 205, 218, 232, 247, 7, 24, 42, 61, 81, 102, 124, 147, 171, 196, 222, 249, 21, 50, 80, 111, 143, 176, 210, 245, 25, 62, 100, 139, 179, 220, 6, 49, 93, 138, 184, 231, 23, 72, 122, 173, 225, 22, 76, 131, 187, 244, 46, 105, 165, 226, 32, 95, 159, 224, 34, 101, 169, 238, 52, 123, 195, 12, 86, 161, 237, 58, 136, 215, 39, 120, 202, 29, 113, 198, 28, 115, 203, 36, 126, 217, 53, 146, 240, 79, 175, 16, 114, 213, 57, 158, 4, 107, 211, 60, 166, 17, 125, 234, 88, 199, 55, 168, 26, 141, 1, 118, 236, 99, 219, 84, 206, 73, 197, 66, 192, 63, 191, 64, 194, 69, 201, 78, 212, 91, 227, 108, 246, 129, 13, 154, 40, 183, 71, 216, 106, 253, 145, 38, 188, 83, 235, 132, 30, 185, 85, 242, 144, 47, 207, 112, 18, 181, 89, 254, 164, 75, 243, 156, 70, 241, 157, 74, 248, 167, 87, 8, 186, 109, 33, 214, 140, 67, 251, 180, 110, 41, 229, 162, 96, 31, 223, 160, 98, 37, 233, 174, 116, 59, 3, 204, 150, 97, 45, 250, 200, 151, 103, 56, 10, 221, 177, 134, 92, 51, 11, 228, 190, 153, 117, 82, 48, 15, 239, 208, 178, 149, 121, 94, 68, 43, 19, 252, 230, 209, 189, 170, 152, 135, 119, 104, 90, 77, 65, 54, 44, 35, 27, 20, 14, 9, 5, 2, 0, 255, 255, 0]
[2, 5, 9, 14, 20, 27, 35, 44, 54, 65, 77, 90, 104, 119, 135, 152, 170, 189, 209, 230, 252, 19, 43, 68, 94, 121, 149, 178, 208, 239, 15, 48, 82, 117, 153, 190, 228, 11, 51, 92, 134, 177, 221, 10, 56, 103, 151, 200, 250, 45, 97, 150, 204, 3, 59, 116, 174, 233, 37, 98, 160, 223, 31, 96, 162, 229, 41, 110, 180, 251, 67, 140, 214, 33, 109, 186, 8, 87, 167, 248, 74, 157, 241, 70, 156, 243, 75, 164, 254, 89, 181, 18, 112, 207, 47, 144, 242, 85, 185, 30, 132, 235, 83, 188, 38, 145, 253, 106, 216, 71, 183, 40, 154, 13, 129, 246, 108, 227, 91, 212, 78, 201, 69, 194, 64, 191, 63, 192, 66, 197, 73, 206, 84, 219, 99, 236, 118, 1, 141, 26, 168, 55, 199, 88, 234, 125, 17, 166, 60, 211, 107, 4, 158, 57, 213, 114, 16, 175, 79, 240, 146, 53, 217, 126, 36, 203, 115, 28, 198, 113, 29, 202, 120, 39, 215, 136, 58, 237, 161, 86, 12, 195, 123, 52, 238, 169, 101, 34, 224, 159, 95, 32, 226, 165, 105, 46, 244, 187, 131, 76, 22, 225, 173, 122, 72, 23, 231, 184, 138, 93, 49, 6, 220, 179, 139, 100, 62, 25, 245, 210, 176, 143, 111, 80, 50, 21, 249, 222, 196, 171, 147, 124, 102, 81, 61, 42, 24, 7, 247, 232, 218, 205, 193, 182, 172, 163, 155, 148, 142, 137, 133, 130, 128, 127, 127, 128, 130, 133, 137, 142, 148, 155, 163, 172, 182, 193, 205, 218, 232, 247, 7, 24, 42, 61, 81, 102, 124, 147, 171, 196, 222, 249, 21, 50, 80, 111, 143, 176, 210, 245, 25, 62, 100, 139, 179, 220, 6, 49, 93, 138, 184, 231, 23, 72, 122, 173, 225, 22, 76, 131, 187, 244, 46, 105, 165, 226, 32, 95, 159, 224, 34, 101, 169, 238, 52, 123, 195, 12, 86, 161, 237, 58, 136, 215, 39, 120, 202, 29, 113, 198, 28, 115, 203, 36, 126, 217, 53, 146, 240, 79, 175, 16, 114, 213, 57, 158, 4, 107, 211, 60, 166, 17, 125, 234, 88, 199, 55, 168, 26, 141, 1, 118, 236, 99, 219, 84, 206, 73, 197, 66, 192, 63, 191, 64, 194, 69, 201, 78, 212, 91, 227, 108, 246, 129, 13, 154, 40, 183, 71, 216, 106, 253, 145, 38, 188, 83, 235, 132, 30, 185, 85, 242, 144, 47, 207, 112, 18, 181, 89, 254, 164, 75, 243, 156, 70, 241, 157, 74, 248, 167, 87, 8, 186, 109, 33, 214, 140, 67, 251, 180, 110, 41, 229, 162, 96, 31, 223, 160, 98, 37, 233, 174, 116, 59, 3, 204, 150, 97, 45, 250, 200, 151, 103, 56, 10, 221, 177, 134, 92, 51, 11, 228, 190, 153, 117, 82, 48, 15, 239, 208, 178, 149, 121, 94, 68, 43, 19, 252, 230, 209, 189, 170, 152, 135, 119, 104, 90, 77, 65, 54, 44, 35, 27, 20, 14, 9, 5, 2, 0, 255, 255, 0]
```

On remarque assez rapidement qu'en supprimant la partie permutation de l'algorithme, on retrouve une certaine linéarité dans la suite générée. Donc `it+512 = it` et `jt+512 = jt`.

La période est ici de 512 car le modulo la suite générée est de taille 512 avant de retrouver la même suite.

Cette linéarité s'explique simplement par le fait que le modulo 256 va réinitialiser la valeur de `i` et influer directement la valeur de `j` qui va également reprendre sa valeur initiale.
